<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 */
class Product
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $product_code;

    /**
     * @ORM\Column(type="decimal", precision=12, scale=2)
     */
    private $price;

    private $name;

    /**
     * @ORM\OneToMany(targetEntity="ProductDescription", mappedBy="productDescription")
     */
    private $product_description;

    public function __construct()
    {
        $this->product_description = new ArrayCollection();
    }

    /**
     * @return Collection|ProductDescription[]
     */
    public function getProductDescription(): Collection
    {
        return $this->product_description;
    }

    public function setProductDescription(ProductDescription $productDescription)
    {
        $this->product_description = $productDescription;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProduct_Code(): ?string
    {
        return $this->product_code;
    }

    public function setProduct_Code(string $product_code): self
    {
        $this->product_code = $product_code;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
