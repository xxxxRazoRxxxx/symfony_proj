<?php
namespace App\Controller;

use App\Entity\Product;
use App\Entity\ProductDescription;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProductsController extends AbstractController
{
    private $lang_code = 'en';

    /**
     * @Route("/symfony_proj/products", methods={"GET"})
     */
    function view()
    {
        $request = Request::createFromGlobals();

        $params = $request->query->all();

        $this->lang_code = isset($params['lang_code']) ? $params['lang_code'] : $this->lang_code;
        unset($params['lang_code']);

        if (!empty($params)){
            $products = $this->getDoctrine()
                ->getRepository(Product::class)
                ->findBy([], [$params['sort_by'] => $params['type']]);
        } else {
            $products = $this->getDoctrine()->getRepository(Product::class)->findAll();
        }

        foreach ($products as $product) {
            $product_description = $this->getDoctrine()
                ->getRepository(ProductDescription::class)
                ->findOneBy(['product_id' => $product->getId(), 'lang_code' => $this->lang_code]);

            $product->setName($product_description->getName());
        }

        if ($this->lang_code == 'ru') {
            $lang_code = 'en';
        } else {
            $lang_code = 'ru';
        }

        return $this->render('products/view.html.twig', [
            'products' => $products,
            'lang_code' => $lang_code
        ]);
    }

    /**
     * @Route("/symfony_proj/products/{id}", methods={"GET"})
     */
    function show(int $id)
    {
        $product = $this->getDoctrine()->getRepository(Product::class)->find($id);

        $product_description = $this->getDoctrine()
            ->getRepository(ProductDescription::class)
            ->findOneBy(['product_id' => $product->getId(), 'lang_code' => $this->lang_code]);

        $product->setName($product_description->getName());

        return $this->render('products/show.html.twig', [
            'product' => $product,
        ]);
    }
}